import { connect } from 'mongoose'
// bodyParser is a middleware to handdle http requests in express.js
import bodyParser from 'body-parser' 
//
import express from 'express'
// UserModel is the users schema which we need to import in order to get the datas from db
import UserModel from './UserModel'
import cors from 'cors'

// the url we use to connect with mongoDB
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())


app.get('/', (req, res) => {
  res.send('Just a test')
})

// Gets allt he users that are in database
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

// Creates a new user
app.post('/users', (req, res) => {

  let user = new UserModel()


  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password


  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})



// Alina's code

// Update User 
app.put('/users/:id', (req, res) => {
  const user = await UserModel.findById(req.user._id)

  
  if (user) {
    user.email = req.body.email 
    user.firstName = req.body.firstName 
    user.lastName = req.body.lastName 
    if(req.body.password){
      user.password = req.body.password
    }

    const updatedUser = await user.save()

    res.send({
        _id: updatedUser._id,
        name: updatedUser.name,
        surname: updatedUser.surname,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        token: generateToken(updatedUser._id),
      })
   
     } else {
       res.status(404)
       throw new Error('User not found')
     }
})




// Delete user 

app.delete('/users/:id', (req, res) => {
  let user = await UserModel.findById(req.params.id)

  if(user){
    await user.remove()
    res.json({message: "User Deleted"})
  } else{
    res.status(404)
    throw new Error('User not found')
  }
  res.json(users)
})

// shows the port in which tha app runs
app.listen(8080, () => console.log('Example app listening on port 8080!'))
